﻿using Calendar.Localization;
using MetroLab.Common;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace Calendar.ViewModel
{
    [DataContract]
    public class PfDataLoadViewModel : DataLoadViewModel
    {
        private ICommand _tryAgainCommand;
        [IgnoreDataMember]
        public ICommand TryAgainCommand {
            get { return GetCommand(ref _tryAgainCommand, o => AppViewModel.UpdateGlobalIsWorkingOffline()); }
        }

        private ICommand _updateCommand;
        [IgnoreDataMember]
        public ICommand UpdateCommand {
            get { return GetCommand(ref _updateCommand, o => UpdateAsync()); }
        }
        
        protected override async Task<bool> TryLoadAsyncInner(Func<Task<bool>> loadAction)
        {
            UICommandInvokedHandler goBackAction = o => AppViewModel.Current.GoBack();

            try
            {
                if (_isContentActual) return true;
                IsWorkingOffline = false;
                IsWasProblemsWithConnection = false;
                var isLoadedSuccessfully = await loadAction();
                return isLoadedSuccessfully;
            }
            catch (Exception ex)
            {
                var message = ex.ToString();
                if (Debugger.IsAttached) Debugger.Break();
                else message = LocalizedResources.GetString("msg_GeneralUnhandledExceptionTryAgain");

                RunLoadFailed(message, true, goBackAction);
                return false;
            }
        }
    }
}
