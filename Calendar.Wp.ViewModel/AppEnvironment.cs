﻿using System.Reflection;
using Windows.Storage;
using Windows.UI.Xaml;

namespace Calendar.ViewModel
{
    public class AppEnvironment
    {
        public static readonly string AppVersion;
        public static readonly string AppShortVersion;

        static AppEnvironment()
        {
            dynamic type = Application.Current.GetType();
            var version = (new AssemblyName(type.Assembly.FullName)).Version;

            AppShortVersion = string.Format("{0}.{1}", version.Major, version.Minor);
            AppVersion = version.ToString();
        }

        public static bool IsPurchased
        {
            get { return GetSettingByKey("IsPurchased", false); }
            set { SetSettingByKey("IsPurchased", value); }
        }

        private static T GetSettingByKey<T>(string key, T defaultValue)
        {
            var values = ApplicationData.Current.LocalSettings.Values;
            return values.ContainsKey(key) && values[key] is T ? (T)values[key] : defaultValue;
        }

        private static void SetSettingByKey(string key, object value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value;
        }
    }
}
