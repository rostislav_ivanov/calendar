﻿using System.Diagnostics;
using System.Windows.Input;
using Windows.ApplicationModel.Store;
using Calendar.Localization;
using System;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Streams;
using MetroLab.Common;

namespace Calendar.ViewModel
{
    public enum TileItemTemplateKeyEnum : byte { Other, First, Last };

    public class MainPageItem
    {
        public int Day { get; set; }
        public TileItemTemplateKeyEnum Key { get; set; }
    }

    [DataContract]
    public class MainPageViewModel : PageViewModel
    {
        private const int CountItems = 7;
        private const string PurchaseKey = "Prophet";

        private MainPageItem[] _items;
        [IgnoreDataMember]
        public MainPageItem[] Items
        {
            get
            {
                if (_items != null && !AppEnvironment.IsPurchased) return _items;

                var canChangeSelectedIndex = _items == null;
                var now = DateTime.Now;
                var day = now.DayOfYear; 
                int[] items;

                if (AppEnvironment.IsPurchased)
                {
                    var сountDays = (new DateTime(now.Year, 12, 31)).DayOfYear;

                    items = new int[сountDays + 2];
                    for (int i = - 2; i < сountDays; i++)
                        items[i + 2] = i + 1;

                    _items = items.Select(i => new MainPageItem { Day = i }).ToArray();
                    if (canChangeSelectedIndex) SelectedIndex = day;
                }
                else
                {                    
                    items = new int[CountItems];
                    for (int i = 0; i < CountItems; i++)
                        items[i] = day - CountItems + i + 1;
                    
                    _items = items.Select(i => new MainPageItem { Day = i}).ToArray();
                    _items[0].Key = TileItemTemplateKeyEnum.First;
                    _items[CountItems - 1].Key = TileItemTemplateKeyEnum.Last;                    
                    SelectedIndex = CountItems - 2;
                }

                return _items;
            } 
        }

        public static bool IsFirstLoading = true;

        private string _futureDaysWindowContent;
        public string FutureDaysWindowContent
        {
            get
            {
                if (!string.IsNullOrEmpty(_futureDaysWindowContent))
                    return _futureDaysWindowContent;

                var year = DateTime.Now.Year;
                var items = Items;
                var date = new DateTime(year, 1, 1).AddDays(items[items.Length - 1].Day);
                return _futureDaysWindowContent = String.Format(LocalizedResources.GetString("FutureDaysWindowString"), date.ToString("d MMMM", CultureInfo.CurrentUICulture));
            }
        }

        public string PastDaysWindowContent { get {
            return LocalizedResources.GetString("PastDaysWindowString"); }
        }

        private bool _isAppBarClosedByUser=true;

        public bool IsAppBarClosedByUser
        {
            get { return _isAppBarClosedByUser; }
            set
            {
                if (SetProperty(ref _isAppBarClosedByUser, value))
                {
                    OnPropertyChanged("IsAppBarVisible");
                }
            }
        }

        private bool _isAppBarVisible;

        public bool IsAppBarVisible { get {
            return (_selectedIndex != 0) && (_selectedIndex != Items.Length - 1) && !IsAppBarClosedByUser; }           
        }
 
        private int _selectedIndex;
        [DataMember]
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (SetProperty(ref _selectedIndex, value))
                    OnPropertyChanged("IsAppBarVisible");
            }
        }

        [IgnoreDataMember]
        public bool IsPurchased {
            get
            {
                var isPurchased = AppEnvironment.IsPurchased;
                if (!isPurchased)
                {
                    try
                    {
#if DEBUG
                        var license = CurrentAppSimulator.LicenseInformation;
#else
                        var license = CurrentApp.LicenseInformation;
#endif
                        var removeAdLicence = license.ProductLicenses[PurchaseKey];
                        isPurchased = removeAdLicence.IsActive;
                        AppEnvironment.IsPurchased = isPurchased;
                    }
                    catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
                }
                return isPurchased;
            }
        }

        public async Task<bool> Purchase()
        {
            try
            {
#if DEBUG
                await CurrentAppSimulator.RequestProductPurchaseAsync(PurchaseKey);
#else
                await CurrentApp.RequestProductPurchaseAsync(PurchaseKey);
#endif
                OnPropertyChanged("IsPurchased");
                if (IsPurchased) OnPropertyChanged("Items");
            }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
            
            return AppEnvironment.IsPurchased;
        }


#if WINDOWS_PHONE_APP
        private ICommand _shareCommand;
        [IgnoreDataMember]
        public ICommand ShareCommand { get { return GetCommand(ref _shareCommand, 
            o => DataTransferManager.ShowShareUI()); } }

        private ICommand _settingsCommand;
        [IgnoreDataMember]
        public ICommand SettingsCommand { get { return GetCommand(ref _settingsCommand, 
            o => AppViewModel.Current.NavigateToViewModel(new SettingsPageViewModel())); } }
#endif

        public override async Task RequestData(DataTransferManager manager, DataRequestedEventArgs args)
        {
            if (Items == null || SelectedIndex < 1 || SelectedIndex >= Items.Length - 1) return;

            var day = Items[SelectedIndex].Day;
            var date = new DateTime(DateTime.Now.Year, 1, 1).AddDays(day);
            var imageUri = new Uri(string.Format("ms-appx:///Assets/Images/{0}_{1}.jpg", 
                date.Month, date.Day.ToString().PadLeft(2, '0')));
            var title = date.ToString("d MMMM", new CultureInfo("ru-Ru"));//LocalizedResources.GetString(string.Format("Day{0}Title", day));
            var description = LocalizedResources.GetDayDescripyionString(title);
            var requestData = args.Request.Data;
            var requestProperties = requestData.Properties;

            requestProperties.Title = title;
            requestProperties.Description = description;
            requestData.SetText(string.Format("{0}\r\n{1}", title, description));
            requestProperties.ApplicationName = LocalizedResources.GetString("ApplicationName");

            var storageFile = await StorageFile. GetFileFromApplicationUriAsync(imageUri);
            var streamReference = RandomAccessStreamReference.CreateFromStream(await storageFile.OpenAsync(FileAccessMode.Read));
            requestProperties.Thumbnail = streamReference;
            requestData.SetBitmap(streamReference);

            var inMemoryStream = new InMemoryRandomAccessStream();
            var imageDecoder = await BitmapDecoder.CreateAsync(await storageFile.OpenAsync(FileAccessMode.Read));
            var imageEncoder = await BitmapEncoder.CreateForTranscodingAsync(inMemoryStream, imageDecoder);
            await imageEncoder.FlushAsync();
            requestData.SetData(StandardDataFormats.Bitmap, RandomAccessStreamReference.CreateFromStream(inMemoryStream));

            requestData.SetStorageItems(new IStorageItem[] { storageFile });
        }
    }
}
