﻿using MetroLab.Common;
using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.Storage.Streams;

namespace Calendar.ViewModel
{
#if WINDOWS_PHONE_APP
    [KnownType(typeof(SettingsPageViewModel))]
#endif
    [KnownType(typeof(MainPageViewModel))]
    [DataContract]
    public abstract class PageViewModel : PfDataLoadViewModel, IPageViewModel
    {
        protected PageViewModel()
        {            
#if DEBUG
            _itemsInMemoryCount++;
            GC.Collect();
#endif
        }

#if DEBUG
        private static int _itemsInMemoryCount;

        ~PageViewModel()
        {
            _itemsInMemoryCount--;
            System.Diagnostics.Debug.WriteLine("Destructed: {0} {1}", this, Guid);
        }
#endif

        public virtual void OnNavigating()
        {
            // OfflineComments:
            //if (IsWorkingOffline)
            //    UpdateAsync();
        }

        public IPrintHelper PrintHelper { get; set; }
        public virtual async Task RequestData(DataTransferManager manager, DataRequestedEventArgs args){ }
        
        protected async Task<IRandomAccessStream> GetStreamByLocalUri(Uri localUri)
        {
            var imageStream = new InMemoryRandomAccessStream();
            var resultStorageFile = await StorageFile.GetFileFromApplicationUriAsync(localUri);
            await RandomAccessStream.CopyAsync(await resultStorageFile.OpenSequentialReadAsync(), imageStream);
            await imageStream.FlushAsync();
            return imageStream;
        }

        public virtual async Task OnSerializingToStorageAsync() { }

        public virtual async Task OnSerializedToStorageAsync() { }

        public virtual async Task OnDeserializedFromStorageAsync() { }

        public virtual async Task OnNavigatingAsync() { }
    }
}
