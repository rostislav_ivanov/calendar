﻿using System.Linq;
using Calendar.View.UserControls;
using Calendar.ViewModel;
using System;
using System.Diagnostics;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Calendar.View
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
           
            SizeChanged += (s, e) =>
            {
                if (ApplicationView.GetForCurrentView().IsFullScreen)
                {
                    SplashImage.Visibility = Visibility.Collapsed;
                    return;
                }
                    
                // place for image and flipview arrows
                var minWidth = Window.Current.Bounds.Height*0.6 + 140;
                SplashImage.Visibility = e.NewSize.Width <= minWidth ? Visibility.Visible : Visibility.Collapsed;
            };

            //Note: This code resolves problem with flipview blinking when SelectedIndex is at first 0
            InnerFlipView.Loaded += (s, e) =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
                Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
                {
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
                    InnerFlipView.Opacity = 1;
                });

            };

            //Note: This code resolves problem with flipview blinking when SelectedIndex is at first 0
            InnerFlipView.Unloaded += (s, e) =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
                Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
                {
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.OneWay });
                    InnerFlipView.SelectedIndex = 0;
                    InnerFlipView.Opacity = 0;
                });
            };
        }

        private void OnTapped(object sender, TappedRoutedEventArgs e)
        {
            var grid = sender as Grid;
            if (grid == null) return;
            grid.Tapped -= OnTapped;
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            scrollViewer.ChangeView(null, null, scrollViewer.MinZoomFactor);
        }

        private void FlipViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                if (!AppEnvironment.IsPurchased) return;

                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
            
                var index = vm.SelectedIndex;
                var endIndex = vm.Items.Length - 1;
                if (index != 0 && index != endIndex) return;

                var year = DateTime.Now.Year;
                var first = new DateTime(year, 1, 1).Date;
                var last = new DateTime(year, 12, 31).Date;
                var date = new DateTime(year, 1, 1).AddDays(vm.Items[index].Day).Date;
                if (!(date.Month == first.Month && date.Day == first.Day)
                    && !(date.Month == last.Month && date.Day == last.Day)) return;
            
                InnerFlipView.SelectedIndex = index == 0 ? endIndex - 1 : 1;
                InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                    new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
            });
        }

        private async void SuggestRateApp()
        {
            try { await new RatingsHelper().InitialiseAsync(); }
            catch (Exception) { }
        }

        protected override void OnViewModelLoadedOrUpdatedSuccessfully()
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                try
                {
                    if (MainPageViewModel.IsFirstLoading)
                    {
                        SuggestRateApp();
                        MainPageViewModel.IsFirstLoading = false;
                    }
                }
                catch (Exception e)
                {
                    Debugger.Break();
                }
            });
        }

        private void ButtonNoOnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                var index = InnerFlipView.SelectedIndex;
                InnerFlipView.SelectedIndex = index == 0 ? 1 : index - 1;
                InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                    new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
            });
        }

        private void ButtonYesOnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
                    
                InnerFlipView.SelectionChanged -= FlipViewSelectionChanged;
                var selectedIndexDay = vm.Items[InnerFlipView.SelectedIndex].Day + 1; 
                var isPurchased = await vm.Purchase();
                if (isPurchased)
                {
                    InnerFlipView.SelectedIndex = selectedIndexDay;
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });                    
                }
                InnerFlipView.SelectionChanged += FlipViewSelectionChanged;
            });
        }
    }
}

