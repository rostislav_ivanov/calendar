﻿using Calendar.BackgroundAgent;
using Calendar.Localization;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace Calendar.View.UserControls
{
    public sealed partial class SettingsControl
    {
        public SettingsControl()
        {
            InitializeComponent();

            TextBlockTitle.Text = LocalizedResources.GetString("Settings");
            TimePicker.Time = AppStorageSettings.StorageNotificationTime;
            TimePicker.TimeChanged += (s, e) =>
            {
                AppStorageSettings.StorageNotificationTime = e.NewTime;
                UpdateApplicationTileTask.SetUpPushNotification(false);
            };
        }

        public bool IsPushNotificationEnbaled {
            get { return AppStorageSettings.IsPushNotificationEnbaled; }
            set { AppStorageSettings.IsPushNotificationEnbaled = value; }
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            var parent = Parent as Popup;
            if (parent != null) parent.IsOpen = false;
            
            SettingsPane.Show();
        }
    }
}