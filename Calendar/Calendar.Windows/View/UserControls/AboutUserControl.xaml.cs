﻿using Calendar.Localization;
using Calendar.ViewModel;
using System;
using System.Diagnostics;
using Windows.System;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;

namespace Calendar.View.UserControls
{
    public sealed partial class AboutUserControl
    {
        public AboutUserControl()
        {
            InitializeComponent();

            TextBlockTitle.Text = LocalizedResources.GetString("About");
            //TextBlockApplication.Text = LocalizedResources.GetString("ApplicationName");
            TextBlockVersion.Text = string.Format("{0} {1}", LocalizedResources.GetString("Version"),
                AppEnvironment.AppShortVersion);

            BtnLeaveComment.Content = LocalizedResources.GetString("LeaveComment");
            BtnContactUs.Content = LocalizedResources.GetString("ContactUs");
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            var parent = Parent as Popup;
            if (parent != null) parent.IsOpen = false;
            
            SettingsPane.Show();
        }

        private async void ContactUsOnClick(object sender, RoutedEventArgs e)
        {
            try { await RatingsHelper.SendFeedback(); }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        private async void SendFeedbackOnClick(object sender, RoutedEventArgs e)
        {
            await RatingsHelper.MakeReview();
        }
    }
}