﻿using Calendar.Localization;
using Calendar.ViewModel;
using MetroLab.Common;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Resources;
using Windows.ApplicationModel.Store;
using Windows.System;
using Windows.UI.Popups;

namespace Calendar.View.UserControls
{
    public enum FeedbackState { Inactive, FirstReview, SecondReview, Feedback }

    /// <summary>
    /// This helper class controls the behaviour of the FeedbackOverlay control
    /// When the app has been launched 5 times the initial prompt is shown
    /// If the user reviews no more prompts are shown
    /// When the app has bee launched 10 times and not been reviewed, the prompt is shown
    /// </summary>
    public class RatingsHelper
    {
        // http://msdn.microsoft.com/en-us/library/windows/apps/hh974767.aspx

#if WINDOWS_PHONE_APP
        private const string AppUri = "ms-windows-store:reviewapp?appid=45f320cc-529b-4380-9b98-240bb9b3b01d";
#else 
        private const string AppUri = "ms-windows-store:REVIEW?PFN=45095MaximBurtsev.364-_8efpa1qj3csrt";
#endif


        private const string LaunchCount = "LAUNCH_COUNT";
        private const string Reviewed = "REVIEWED";
        private const string SupportMail = "mailto:{0}?subject={1}&body={2}";
        private const int FirstCount = 3;
        private const int SecondCount = 8;

        private int _launchCount;
        private bool _reviewed;
        private bool _isTrial;
        private string _message;
        private string _title;
        private string _yesText;
        private string _noText;

        private FeedbackState _state = FeedbackState.Inactive;
        private static ResourceLoader _resLoader;

        /// <summary>
        /// Loads last state from storage and works out the new state
        /// </summary>
        private void LoadState()
        {
            try
            {
                // Load counters from storage
                _launchCount = SettingsStorageHelper.GetSetting<int>(LaunchCount);
                _reviewed = SettingsStorageHelper.GetSetting<bool>(Reviewed);

                // Check trial state
                _isTrial = CurrentApp.LicenseInformation.IsActive && CurrentApp.LicenseInformation.IsTrial;

                if (!_reviewed)
                {
                    _launchCount++;

                    if (_launchCount == FirstCount)
                        _state = FeedbackState.FirstReview;
                    else if (_launchCount == SecondCount)
                        _state = FeedbackState.SecondReview;

                    StoreState();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FeedbackHelper.LoadState - Failed to load state, Exception: {0}", ex);
            }
        }

        /// <summary>
        /// Stores current state
        /// </summary>
        private void StoreState()
        {
            try
            {
                SettingsStorageHelper.SetSetting(LaunchCount, _launchCount);
                SettingsStorageHelper.SetSetting(Reviewed, _reviewed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FeedbackHelper.StoreState - Failed to store state, Exception: {0}", ex);
            }
        }

        /// <summary>
        /// Entry point to initialise and start rating workflow
        /// </summary>
        /// <returns></returns>
        public async Task InitialiseAsync()
        {
            LoadState();

            // Only continue if not trial
            if (_isTrial) return;

            // Uncomment for testing
            //_state = FeedbackState.FirstReview;
            // _state = FeedbackState.SecondReview;
            // _state = FeedbackState.Feedback;

            if (_state == FeedbackState.FirstReview)
            {
                _title = GetResource("RatingTitle");
                _message = GetResource("RatingMessage1");
                _yesText = GetResource("RatingYes");
                _noText = GetResource("RatingNo");

                await ShowMessageAsync();
            }
            else if (_state == FeedbackState.SecondReview)
            {
                _title = GetResource("RatingTitle");
                _message = GetResource("RatingMessage2");
                _yesText = GetResource("RatingYes");
                _noText = GetResource("RatingNo");

                await ShowMessageAsync();
            }
        }

        private async Task ShowMessageAsync()
        {
            var md = new MessageDialog(_message, _title);
            var result = 0;

            md.Commands.Add(new UICommand(_yesText, a => result = 1));
            md.Commands.Add(new UICommand(_noText, a => result = 2));
            md.DefaultCommandIndex = 0;

            await md.ShowAsync();

            if (result == 1) await OnYesClickAsync();
            else await OnNoClickAsync();
        }

        private async Task OnNoClickAsync()
        {
            if (_state == FeedbackState.FirstReview)
            {
                _title = GetResource("FeedbackTitle");
                _message = GetResource("FeedbackMessage1");
                _yesText = GetResource("FeedbackYes");
                _noText = GetResource("FeedbackNo");

                _state = FeedbackState.Feedback;
                await ShowMessageAsync();
            }
        }

        private async Task OnYesClickAsync()
        {
            if (_state == FeedbackState.FirstReview || _state == FeedbackState.SecondReview)
            {
                _reviewed = true;
                StoreState();
                await Launcher.LaunchUriAsync(new Uri(AppUri, UriKind.Absolute));
            }
            else if (_state == FeedbackState.Feedback)
                await SendFeedback();
        }

        public static async Task MakeReview() 
        {
            SettingsStorageHelper.SetSetting(Reviewed, true);
            await Launcher.LaunchUriAsync(new Uri(AppUri, UriKind.Absolute));
        }

        public static async Task SendFeedback()
        {
            var package = Package.Current;
            var packageId = package.Id;
            var version = packageId.Version;

            var pkgInfo = String.Format(
                                "Name: \"{0}\"\n" +
                                "Version: {1}.{2}.{3}.{4}\n" +
                                "Architecture: {5}\n" +
                                "ResourceId: \"{6}\"\n" +
                                "Publisher: \"{7}\"\n" +
                                "PublisherId: \"{8}\"\n" +
                                "FullName: \"{9}\"\n" +
                                "FamilyName: \"{10}\"\n" +
                                "IsFramework: {11}",
                                packageId.Name,
                                version.Major, version.Minor, version.Build, version.Revision,
                                packageId.Architecture,
                                packageId.ResourceId,
                                packageId.Publisher,
                                packageId.PublisherId,
                                packageId.FullName,
                                packageId.FamilyName,
                                package.IsFramework);
            
            // Body text including hardware, firmware and software info
            var body = string.Format(GetResource("FeedbackBody"), pkgInfo);

            var subject = string.Format("{0}: {1} {2} {3}", GetResource("FeedbackSubject"), 
                LocalizedResources.GetString("Version"), AppEnvironment.AppShortVersion,
#if WINDOWS_PHONE_APP
                "Windows Phone");
#else 
                "Windows 8");
#endif                

            var email = Uri.EscapeUriString(string.Format(SupportMail,
                GetResource("FeedbackTo"), subject, body));

            await Launcher.LaunchUriAsync(new Uri(email, UriKind.Absolute));            
        }

        private static string GetResource(string name)
        {
            if (_resLoader == null)
                _resLoader = ResourceLoader.GetForViewIndependentUse("Resources");

            return _resLoader.GetString(name);
        }
    }
}
