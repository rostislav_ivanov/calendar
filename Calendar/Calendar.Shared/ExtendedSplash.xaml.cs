﻿using System;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Calendar
{
    public sealed partial class ExtendedSplash
    {
        private DispatcherTimer _showWindowTimer;

        public ExtendedSplash(SplashScreen splashscreen)
        {
            InitializeComponent();

#if WINDOWS_APP
            if (splashscreen == null) return;

            _splash = splashscreen;
            _splashImageRect = splashscreen.ImageLocation;
            ProgressRing.Margin = new Thickness(0, 260, 0, 0);
            ProgressRing.VerticalAlignment = VerticalAlignment.Center;
            PositionImage();
            Win8Image.Visibility = Visibility.Visible;
            ExtendedSplashImage.ImageOpened += ExtendedSplashImageImageOpened;
            Window.Current.SizeChanged += CurrentOnSizeChanged;
#else
            ProgressRing.MaxWidth = 45;
            ProgressRing.MinWidth = 45;
            ProgressRing.MaxHeight = 45;
            ProgressRing.MinHeight = 45;
            ProgressRing.Margin = new Thickness(0,  0, 0, 180);
            ProgressRing.VerticalAlignment = VerticalAlignment.Bottom;
            WP8Image.Visibility = Visibility.Visible;
            WP8Image.ImageOpened += ExtendedSplashImageImageOpened;
#endif
        }

#if WINDOWS_APP
        private readonly SplashScreen _splash; // Variable to hold the splash screen object.
        private Rect _splashImageRect; // Rect to store splash screen image coordinates.

        /// <summary>
        /// Safely update the extended splash screen image coordinates. 
        /// This function will be fired in response to snapping, unsnapping, rotation, etc...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="windowSizeChangedEventArgs"></param>
        private void CurrentOnSizeChanged(object sender, WindowSizeChangedEventArgs windowSizeChangedEventArgs)
        {
            if (_splash == null) return;
            
            // Update the coordinates of the splash screen image.
            _splashImageRect = _splash.ImageLocation;
            PositionImage();            
        }

        /// <summary>
        /// Position the extended splash screen image in the same location as the system splash screen image.
        /// </summary>
        private void PositionImage()
        {
            ExtendedSplashImage.SetValue(Canvas.LeftProperty, _splashImageRect.X);
            ExtendedSplashImage.SetValue(Canvas.TopProperty, _splashImageRect.Y);
            ExtendedSplashImage.Height = _splashImageRect.Height;
            ExtendedSplashImage.Width = _splashImageRect.Width;
            
            // Position the extended splash screen's progress ring.
            //ProgressRing.SetValue(Canvas.TopProperty, _splashImageRect.Y + _splashImageRect.Height + 32);
            //ProgressRing.SetValue(Canvas.LeftProperty, _splashImageRect.X + (_splashImageRect.Width / 2) - 15);
        }
#endif

        private void OnShowWindowTimer(object sender, object e)
        {
            _showWindowTimer.Tick -= OnShowWindowTimer;
            _showWindowTimer.Stop();

            // Activate/show the window, now that the splash image has rendered
            Window.Current.Activate();
        }

        private void ExtendedSplashImageImageOpened(object sender, RoutedEventArgs e)
        {
            // ImageOpened means the file has been read, but the image hasn't been painted yet.
            // Start a short timer to give the image a chance to render, before showing the window and starting the animation.
            _showWindowTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(50) };
            _showWindowTimer.Tick += OnShowWindowTimer;
            _showWindowTimer.Start();
        }
    }
}
