﻿using Windows.ApplicationModel.Store;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Calendar.BackgroundAgent;
using Calendar.View;
using Calendar.ViewModel;
using MetroLab.Common;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Calendar.Localization;

#if WINDOWS_APP
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml.Controls.Primitives;
#endif

namespace Calendar
{
    sealed partial class App
    {
        private static readonly Type HomePageViewModelType = typeof(MainPageViewModel);
        private static readonly Type StartViewModelType = typeof(MainPageViewModel);

        private MvvmFrame _mvvmFrame;
        [DataContract]
        private class AppStateSnapshot
        {
            [DataMember]
            public IPageViewModel[] PageViewModels { get; set; }
        }

        private class DataContractViewModelSerializer : IPageViewModelStackSerializer
        {
            private readonly DataContractSerializerSettings _settings;

            public DataContractViewModelSerializer()
            {
                var settings = new DataContractSerializerSettings {
                    KnownTypes = new[] { typeof (BindableBase), typeof (MainPageViewModel),
#if WINDOWS_PHONE_APP
                        typeof (SettingsPageViewModel),
#endif                        
                    },

                    SerializeReadOnlyTypes = false,
                };

                _settings = settings;
            }

            public async Task SerializeAsync(IPageViewModel[] pageViewModels, System.IO.Stream streamForWrite)
            {
                var appStateSnapshot = new AppStateSnapshot { PageViewModels = pageViewModels };
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                serializer.WriteObject(streamForWrite, appStateSnapshot);
                await streamForWrite.FlushAsync();
            }

            public async Task<IPageViewModel[]> DeserializeAsync(System.IO.Stream streamForRead)
            {
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                var appStateSnapshot = (AppStateSnapshot)serializer.ReadObject(streamForRead);
                return appStateSnapshot.PageViewModels;
            }
        }

        public App()
        {
            InitializeComponent();

            UnhandledException += CurrentUnhandledExceptionAsync;
            Suspending += OnSuspendingAsync;

            //ToDo: turn on logging
            //var trace = new FileStreamingTarget();
            //trace.FileNamingParameters.IncludeLevel = true;
            //LogManagerFactory.DefaultConfiguration.AddTarget(LogLevel.Trace, LogLevel.Fatal, trace);
            //LogManagerFactory.DefaultConfiguration.AddTarget(LogLevel.Trace, LogLevel.Fatal, new DebugTarget());
            //// setup the global crash handler...
            //GlobalCrashHandler.Configure();

#if WINDOWS_PHONE_APP
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += (s, e) =>
            {
                AppViewModel.Current.GoBack();
                e.Handled = true;
            };
#else
            RequestedTheme = ApplicationTheme.Light;
#endif
        }

        private static MvvmFrame CreateMainFrame()
        {
            var result = new MvvmFrame(new DataContractViewModelSerializer()) { LoadDataAtBackgroundThread = true, MaxPagesInCacheCount = 5 };
            result.AddSupportedPageViewModel(typeof(MainPageViewModel), typeof(MainPage));
#if WINDOWS_PHONE_APP
            result.AddSupportedPageViewModel(typeof(SettingsPageViewModel), typeof(AboutPage));
#endif
            return result;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (args.PreviousExecutionState != ApplicationExecutionState.Running && args.PreviousExecutionState != ApplicationExecutionState.Suspended)
            {
                var extendedSplash = new ExtendedSplash(args.SplashScreen);
                Window.Current.Content = extendedSplash;

                args.SplashScreen.Dismissed += (sender, o) =>
                    extendedSplash.Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
                    {
                        await InitializeTask;

                        OnLaunchedInnerAsync(args);
                    });
            }
            else OnLaunchedInnerAsync(args);
        }

        protected override async void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);

            await InitializeTask;
            if (args.Kind == ActivationKind.Protocol)
            {
                OnLaunchedInnerAsync(args);
            }
        }

        private async Task<bool> TryNavigateToPinnedPageAsync(IActivatedEventArgs args)
        {
            try
            {
                var launchActivatedEventArgs = args as LaunchActivatedEventArgs;
                if (launchActivatedEventArgs != null && !String.IsNullOrEmpty(launchActivatedEventArgs.Arguments))
                {
                    //PfLinksAgent.Current.TryOpenLink(launchActivatedEventArgs.Arguments);
                    return true;
                }

                var protocolArgs = args as ProtocolActivatedEventArgs;
                if (protocolArgs != null && protocolArgs.Uri != null)
                {
                    //PfLinksAgent.Current.TryOpenLink(protocolArgs.Uri.OriginalString);
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                if (Debugger.IsAttached) Debugger.Break();
                return false;
            }
        }

        private async void OnLaunchedInnerAsync(IActivatedEventArgs args)
        {
#if DEBUG
            if (Debugger.IsAttached)
            {
                DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            AppViewModel.Current.HomePageViewModelType = HomePageViewModelType;// Set startup Page

            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                bool catchedException = false;
                try
                {
                    await _mvvmFrame.LoadFromStorageAsync(true); //restore
                    await TryNavigateToPinnedPageAsync(args);
                }
                catch (Exception e)
                {
                    _mvvmFrame.ClearNavigationStack();
                    //errors in deserializing
#if DEBUG
                    if (Debugger.IsAttached) Debugger.Break();
#endif
                    catchedException = true;
                }
                if (catchedException)
                    await AppViewModel.Current.NavigateToViewModel((PageViewModel)Activator.CreateInstance(StartViewModelType));
            }
            else if ((args.PreviousExecutionState == ApplicationExecutionState.Running) || (args.PreviousExecutionState == ApplicationExecutionState.Suspended))
            {
                if (!await TryNavigateToPinnedPageAsync(args)) Window.Current.Activate();
            }
            else
            {
                if (!await TryNavigateToPinnedPageAsync(args))
                    await AppViewModel.Current.NavigateToViewModel((PageViewModel)Activator.CreateInstance(StartViewModelType));
            }

            Window.Current.Content = _mvvmFrame;
            Window.Current.Activate();
        }

#if USE_FRAME
        private Frame _appFrame;
#endif

        private Task _initializeTask;
        private Task InitializeTask { get { return _initializeTask ?? (_initializeTask = InitializeAsync()); } }

        private async Task InitializeAsync()
        {
            try
            {
                if (!Resources.ContainsKey("AppViewModel"))
                    Resources.Add("AppViewModel", AppViewModel.Current);

                _mvvmFrame = CreateMainFrame();
                AppViewModel.Current.MvvmFrame = _mvvmFrame;

#if WINDOWS_APP
                SettingsPane.GetForCurrentView().CommandsRequested += SettingsPaneCommandsRequested;
#else
                await StatusBar.GetForCurrentView().HideAsync();
#endif

                BackgroundAgentsHelper.RegisterUpdateApplicationTileAgent();



#if DEBUG
                var proxyDataFolder = await Package.Current.InstalledLocation.GetFolderAsync("Assets");
                var proxyFile = await proxyDataFolder.GetFileAsync("TempToDebugInAppPurchaseDeleteLater.xml");
                await CurrentAppSimulator.ReloadSimulatorAsync(proxyFile);
#endif 
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
                throw;
            }
        }

        private void CurrentUnhandledExceptionAsync(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = false;
#if DEBUG
            if (Debugger.IsAttached) Debugger.Break();
            ShowErrorMessage(e.Exception != null ? e.Exception.StackTrace : String.Empty, e.Message ?? string.Empty);
#else 
            ShowErrorMessage(LocalizedResources.GetString("msg_GeneralUnhandledException"),
                LocalizedResources.GetString("txt_Error"));
#endif
        }

        private async void ShowErrorMessage(string content, string title)
        {
            await BaseViewModel.ShowDialogAsync(content, title);
            Current.Exit();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspendingAsync(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await AppViewModel.Current.MvvmFrame.SaveToStorageAsync();
            deferral.Complete();
        }

#if WINDOWS_APP
        private const double SettingsWidth = 346;
        private Popup _settingsPopup;

        private void SettingsPaneCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            var cmdAbout = new SettingsCommand("aboutCommandId", LocalizedResources.GetString("About"),
                x =>
                {
                    _settingsPopup = new Popup();
                    _settingsPopup.Closed += OnSettingsPanePopupClosed;
                    _settingsPopup.Opened += OnSettingsPanePopupOpened;
                    Window.Current.Activated += OnWindowActivated;
                    _settingsPopup.IsLightDismissEnabled = true;
                    _settingsPopup.Width = SettingsWidth;
                    var windowsBounds = Window.Current.Bounds;
                    _settingsPopup.Height = windowsBounds.Height;

                    var mypane = new View.UserControls.AboutUserControl { Height = windowsBounds.Height, Width = 400 };
                    _settingsPopup.Child = mypane;
                    _settingsPopup.SetValue(Canvas.LeftProperty, windowsBounds.Width - mypane.Width);
                    _settingsPopup.SetValue(Canvas.TopProperty, 0);
                    _settingsPopup.IsOpen = true;
                });
            args.Request.ApplicationCommands.Add(cmdAbout);

            var cmdSettings = new SettingsCommand("settingsCommandSettingsId", LocalizedResources.GetString("Settings"),
                x =>
                {
                    _settingsPopup = new Popup();
                    _settingsPopup.Closed += OnSettingsPanePopupClosed;
                    _settingsPopup.Opened += OnSettingsPanePopupOpened;
                    Window.Current.Activated += OnWindowActivated;
                    _settingsPopup.IsLightDismissEnabled = true;
                    _settingsPopup.Width = SettingsWidth;
                    var windowsBounds = Window.Current.Bounds;
                    _settingsPopup.Height = windowsBounds.Height;

                    var mypane = new View.UserControls.SettingsControl { Height = windowsBounds.Height, Width = 400 };
                    _settingsPopup.Child = mypane;
                    _settingsPopup.SetValue(Canvas.LeftProperty, windowsBounds.Width - mypane.Width);
                    _settingsPopup.SetValue(Canvas.TopProperty, 0);
                    _settingsPopup.IsOpen = true;
                });
            args.Request.ApplicationCommands.Add(cmdSettings);

            //var termsOfUseCommand = new SettingsCommand("settingsCommandTermsOfUseId",
            //    LocalizedResources.GetString("SettingsCommandLabelTermsOfUse"),
            //    x => Launcher.LaunchUriAsync(new Uri(ServerXmlUrls.TermsOfUseUriString)));
            //args.Request.ApplicationCommands.Add(termsOfUseCommand);

            //var privacyPolicyCommand = new SettingsCommand("settingsCommandPrivacyPolicyId",
            //    LocalizedResources.GetString("SettingsCommandLabelPrivacyPolicy"),
            //    x => Launcher.LaunchUriAsync(new Uri(ServerXmlUrls.PrivacyPolicyUriString)));
            //args.Request.ApplicationCommands.Add(privacyPolicyCommand);

            //var supportAndLegalCommand = new SettingsCommand("settingsSupportAndLegalId",
            //    LocalizedResources.GetString("SettingsCommandLabelLegalInformation"),
            //    x => Launcher.LaunchUriAsync(new Uri(ServerXmlUrls.LegalUriString)));
            //args.Request.ApplicationCommands.Add(supportAndLegalCommand);

            //var emailSupportCommand = new SettingsCommand("settingsEmailSupportId",
            //    LocalizedResources.GetString("SettingsCommandLabelEmailSupport"),
            //    x => Launcher.LaunchUriAsync(new Uri(string.Concat("mailto:", PfEnvironment.EmailSupport)),
            //        new LauncherOptions { DisplayApplicationPicker = true }));
            //args.Request.ApplicationCommands.Add(emailSupportCommand);
        }

        private void OnSettingsPanePopupOpened(object sender, object e)
        {
            var page = _mvvmFrame.CurrentNavigationItem.GetPage() as MainPage;
            if (page != null) page.OnSettingsPanePopupOpened();
        }

        private void OnSettingsPanePopupClosed(object sender, object e)
        {
            var page = _mvvmFrame.CurrentNavigationItem.GetPage() as MainPage;
            if (page != null) page.OnSettingsPanePopupClosed();

            Window.Current.Activated -= OnWindowActivated;
        }

        private void OnWindowActivated(object sender, WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == CoreWindowActivationState.Deactivated)
                _settingsPopup.IsOpen = false;
        }               
#endif
    }
}
