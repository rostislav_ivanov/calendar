﻿using System;
using Windows.UI.Xaml.Data;

namespace Calendar.Converters
{
    public class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var date = new DateTime(DateTime.Now.Year, 1, 1).AddDays((int)value);
            return string.Format("/Assets/Images/{0}_{1}.jpg", date.Month, date.Day.ToString().PadLeft(2, '0'));
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
