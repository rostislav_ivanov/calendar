﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace Calendar.Converters
{
   public class DoubleToDoubleConverter : IValueConverter
    {
       public double Koeficient { get; set; }
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return Koeficient * (double)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
