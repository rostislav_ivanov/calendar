﻿using Calendar.ViewModel;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Calendar.TemplateSelectors
{
    public class MainPageItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FirstItemTemplate { get; set; }
        public DataTemplate LastItemTemplate { get; set; }
        public DataTemplate OtherItemTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item == null) return null;
            var tileItem = (MainPageItem)item;
            switch (tileItem.Key)
            {
                case TileItemTemplateKeyEnum.First: return FirstItemTemplate;
                case TileItemTemplateKeyEnum.Last: return LastItemTemplate;
                case TileItemTemplateKeyEnum.Other: return OtherItemTemplate;
                default: throw new Exception();
            }
        }
    }
}
