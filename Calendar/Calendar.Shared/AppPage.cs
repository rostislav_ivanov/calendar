﻿using MetroLab.Common;
using System;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Calendar
{
    public class AppPage : MvvmPage
    {
        public AppPage()
        {
#if DEBUG
            _itemsInMemoryCount++;
            GC.Collect();
#endif
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;
            NavigationCacheMode = NavigationCacheMode.Disabled; //NavigationCacheMode.Enabled;
            Transitions = null;           
            SizeChanged += (s, e) =>
            {
                if (TopAppBar == null) return;
                TopAppBar.Visibility = e.NewSize.Width < 500 ? Visibility.Collapsed : Visibility.Visible;                
            };

#if DEBUG && WINDOWS_PHONE_APP
            Loaded += ShowMemoryUsageOnLoaded;
#endif
        }
        
#if DEBUG && WINDOWS_PHONE_APP
        private void ShowMemoryUsageOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= ShowMemoryUsageOnLoaded;

            var textBlock = new TextBlock
            {
                Foreground = new SolidColorBrush(Colors.Red),
                IsHitTestVisible = false,
                FontSize = 15,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Right
            };
            var binding = new Binding { Source = MemoryUsage.Current, Path = new PropertyPath("UserFriendlyString") };
            textBlock.SetBinding(TextBlock.TextProperty, binding);
            var panel = Content as Grid;
            if (panel != null)
            {
                panel.Children.Add(textBlock);
            }
        }
#endif

#if DEBUG
        protected readonly Guid Guid = Guid.NewGuid(); // for searching Memory leaks
        private static int _itemsInMemoryCount;
        
        ~AppPage()
        {
            _itemsInMemoryCount--;
            System.Diagnostics.Debug.WriteLine("Destructed: {0}. Count items in memory = {1}", this, _itemsInMemoryCount);
        }
#endif

#if WINDOWS_PHONE_APP
        protected void EnableAllOrientations(bool isEnabled)
        {
            DisplayInformation.AutoRotationPreferences = isEnabled
                ? (DisplayOrientations.Landscape | DisplayOrientations.LandscapeFlipped 
                    | DisplayOrientations.Portrait | DisplayOrientations.PortraitFlipped) 
                : (DisplayOrientations.Portrait | DisplayOrientations.PortraitFlipped);
        }     
#endif

        public void OnSettingsPanePopupOpened() { }
        public void OnSettingsPanePopupClosed() { }
    }
}
