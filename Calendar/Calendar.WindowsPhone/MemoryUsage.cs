﻿using System;
using System.Diagnostics;
using Windows.System;
using Windows.System.Threading;
using MetroLab.Common;

namespace Calendar
{
#if DEBUG && WINDOWS_PHONE_APP

    public class MemoryUsage : BindableBase
    {
        public readonly static MemoryUsage Current = new MemoryUsage();

        private string _userFriendlyString;

        public string UserFriendlyString
        {
            get { return _userFriendlyString; }
            private set { SetProperty(ref _userFriendlyString, value); }
        }

        private MemoryUsage()
        {
            ThreadPoolTimer.CreatePeriodicTimer(UpdateMemoryUsageTimerHandler, TimeSpan.FromSeconds(1));
        }

        private void UpdateMemoryUsageTimerHandler(ThreadPoolTimer timer)
        {
            try
            {
                GC.Collect();
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
            UserFriendlyString = String.Format(new FileSizeFormatProvider(), "{0:fs}", MemoryManager.AppMemoryUsage);
        }
    }

#endif
}
