﻿using Calendar.Localization;
using Calendar.View.UserControls;
using Calendar.ViewModel;
using MetroLab.Common;
using MetroLab.Common.Converters;
using System;
using System.Diagnostics;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

namespace Calendar.View
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            AddAppBar();

            //Note: This code resolves problem with flipview blinking when SelectedIndex is at first 0
            InnerFlipView.Loaded += (s, e) =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
                Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
                {
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
                    InnerFlipView.Opacity = 1;
                });
            };

            //Note: This code resolves problem with flipview blinking when SelectedIndex is at first 0
            InnerFlipView.Unloaded += (s, e) =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;
                Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
                {
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.OneWay });
                    InnerFlipView.SelectedIndex = 0;
                    InnerFlipView.Opacity = 0;
                });
            };
        }

        private void OnTapped(object sender, TappedRoutedEventArgs e)
        {
            var vm = (MainPageViewModel)ViewModel;
            if(vm!=null)
                vm.IsAppBarClosedByUser = !vm.IsAppBarClosedByUser;
        }

        private CommandBar _bottomAppBar;
        private void AddAppBar()
        {
            _bottomAppBar = new CommandBar
            {
                Visibility = Visibility.Collapsed, RequestedTheme = ElementTheme.Dark,
                Background = new SolidColorBrush(Color.FromArgb(238, 0, 0, 0))
            };
            _bottomAppBar.SetBinding(VisibilityProperty, new Binding
            {
                Path = new PropertyPath("IsAppBarVisible"),
                Converter = new TrueToVisibleConverter()
            });
            var shareButton = new AppBarButton
            {
               Label = LocalizedResources.GetString("AppBarButtonShare"),
               Icon = new BitmapIcon { UriSource = new Uri("ms-appx:///Assets/ico_share.png", UriKind.RelativeOrAbsolute) }
                
            };
            shareButton.SetBinding(ButtonBase.CommandProperty,
                new Binding { Path = new PropertyPath("ShareCommand") });
        
            _bottomAppBar.PrimaryCommands.Add(shareButton);

            var settingsButton = new AppBarButton
            {
                Icon = new SymbolIcon(Symbol.Setting),
                Label = LocalizedResources.GetString("Settings")
            };
            settingsButton.SetBinding(ButtonBase.CommandProperty,
                new Binding { Path = new PropertyPath("SettingsCommand") });

            _bottomAppBar.PrimaryCommands.Add(settingsButton);
            BottomAppBar = _bottomAppBar;
        }

        protected override void OnViewModelLoadedOrUpdatedSuccessfully()
        {
            ThreadPool.RunAsync(o =>
            {
                try
                {
                    Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        async () =>
                        {
                            if (MainPageViewModel.IsFirstLoading)
                            {
                                await new RatingsHelper().InitialiseAsync();
                                MainPageViewModel.IsFirstLoading = false;
                            }
                        });
                }
                catch (Exception e)
                {
                    if (Debugger.IsAttached) Debugger.Break();
                }
            });
        }

        private void FlipViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                if (!AppEnvironment.IsPurchased) return;

                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;

                var index = vm.SelectedIndex;
                var endIndex = vm.Items.Length - 1;
                if (index != 0 && index != endIndex) return;

                var year = DateTime.Now.Year;
                var first = new DateTime(year, 1, 1).Date;
                var last = new DateTime(year, 12, 31).Date;
                var date = new DateTime(year, 1, 1).AddDays(vm.Items[index].Day).Date;
                if (!(date.Month == first.Month && date.Day == first.Day)
                    && !(date.Month == last.Month && date.Day == last.Day)) return;

                InnerFlipView.SelectedIndex = index == 0 ? endIndex - 1 : 1;
                InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                    new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
            });
        }

        private void ButtonNoOnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, () =>
            {
                var index = InnerFlipView.SelectedIndex;
                InnerFlipView.SelectedIndex = index == 0 ? 1 : index - 1;
                InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                    new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
            });
        }

        private void ButtonYesOnClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
            {
                var vm = ViewModel as MainPageViewModel;
                if (vm == null) return;

                InnerFlipView.SelectionChanged -= FlipViewSelectionChanged;
                var selectedIndexDay = vm.Items[InnerFlipView.SelectedIndex].Day + 1;
                var isPurchased = await vm.Purchase();
                if (isPurchased)
                {
                    InnerFlipView.SelectedIndex = selectedIndexDay;
                    InnerFlipView.SetBinding(Selector.SelectedIndexProperty,
                        new Binding { Path = new PropertyPath("SelectedIndex"), Mode = BindingMode.TwoWay });
                }
                InnerFlipView.SelectionChanged += FlipViewSelectionChanged;
            });
        }
    }
}
