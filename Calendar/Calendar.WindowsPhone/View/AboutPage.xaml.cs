﻿using Calendar.BackgroundAgent;
using Calendar.Localization;
using Calendar.View.UserControls;
using Calendar.ViewModel;
using System;
using System.Diagnostics;
using Windows.System;
using Windows.UI.Xaml;

namespace Calendar.View
{
    public sealed partial class AboutPage
    {
        public AboutPage()
        {
            InitializeComponent();

            //TextBlockApplication.Text = LocalizedResources.GetString("ApplicationName");
            TextBlockVersion.Text = string.Format("{0} {1}", LocalizedResources.GetString("Version"),
                AppEnvironment.AppShortVersion);
            
            var settingsText = LocalizedResources.GetString("Settings");
            PivotItemSettings.Header = settingsText.ToLower();
            PivotItemAbout.Header = LocalizedResources.GetString("About").ToLower();
            BtnLeaveComment.Content = LocalizedResources.GetString("LeaveComment").ToLower();
            BtnContactUs.Content = LocalizedResources.GetString("ContactUs").ToLower();

            TimePicker.Time = AppStorageSettings.StorageNotificationTime;
            TimePicker.TimeChanged += (s, e) =>
            {
                AppStorageSettings.StorageNotificationTime = e.NewTime;
                UpdateApplicationTileTask.SetUpPushNotification(false);
            };
        }

        public bool IsPushNotificationEnbaled {
            get { return AppStorageSettings.IsPushNotificationEnbaled; }
            set { AppStorageSettings.IsPushNotificationEnbaled = value; }
        }

        private async void ContactUsOnClick(object sender, RoutedEventArgs e)
        {
            try { await RatingsHelper.SendFeedback(); }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        private async void SendFeedbackOnClick(object sender, RoutedEventArgs e)
        {
            await RatingsHelper.MakeReview();
        }
    }
}