﻿using System;
using System.Diagnostics;
using System.Text;
using Windows.ApplicationModel.Resources;

namespace Calendar.Localization
{
    public static class LocalizedResources
    {
        private static ResourceLoader _resourceLoader;
        private static ResourceLoader _xUidResourceLoader;

        public static string GetString(string resourceName)
        {
            if (_resourceLoader == null)
                _resourceLoader = ResourceLoader.GetForViewIndependentUse("Calendar.Localization/Resources");
            
            var str = _resourceLoader.GetString(resourceName);
            if (String.IsNullOrEmpty(str))
            {
                throw new NullReferenceException(String.Format("Localized resource {0} is null or empty!", resourceName));
            }
            return str;
        }

        public static string GetDayDescripyionString(string resourceName)
        {
            try
            {
                if (_resourceLoader == null)
                    _resourceLoader = ResourceLoader.GetForViewIndependentUse("Calendar.Localization/Resources");

                var str = GetString(resourceName);
                var sb = new StringBuilder();
                var array = str.Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in array)
                {
                    sb.Append(char.ToUpper(item[0]) + item.Substring(1, item.Length - 1) + " ");
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                Debugger.Break();
                return string.Empty;
            }
           
        }

        public static string GetLocalString(string resourceName)
        {
            if (_xUidResourceLoader == null)
                _xUidResourceLoader = ResourceLoader.GetForViewIndependentUse("Resources");
            var str = _xUidResourceLoader.GetString(resourceName);
            return str;
        }
    }
}
