﻿using System.Linq;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;

namespace Calendar.BackgroundAgent
{
    public static class BackgroundAgentsHelper
    {
        private const string TaskEntryPoint = "Calendar.BackgroundAgent.UpdateApplicationTileTask";
        private const string OnTimerUpdater = "Calendar on timer task";
        private const string OnInternetUpdater = "Calendar on internet available task";
        private const string OnMaintenanceUpdater = "Calendar on maintenance timer task";

        public static void RegisterUpdateApplicationTileAgent()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                var taskName = task.Value.Name;
                if (taskName == OnTimerUpdater || taskName == OnInternetUpdater || taskName == OnMaintenanceUpdater)
                    task.Value.Unregister(true);
            }

            if (BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(i => i.Name == OnTimerUpdater) == null)
                RegisterUpdateAppTileTask(OnTimerUpdater, new TimeTrigger(15, false), true);

            if (BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(i => i.Name == OnMaintenanceUpdater) == null)
                RegisterUpdateAppTileTask(OnMaintenanceUpdater, new MaintenanceTrigger(15, false), true);

            if (BackgroundTaskRegistration.AllTasks.Values.FirstOrDefault(i => i.Name == OnInternetUpdater) == null)
                RegisterUpdateAppTileTask(OnInternetUpdater, new SystemTrigger(SystemTriggerType.InternetAvailable, false), false);

            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
            UpdateApplicationTileTask.ExecuteBackgroundTask();
        }

        private static void RegisterUpdateAppTileTask(string name, IBackgroundTrigger trigger, bool isWithInternetCondition)
        {
            var builder = new BackgroundTaskBuilder { Name = name, TaskEntryPoint = TaskEntryPoint };
            builder.SetTrigger(trigger);
            if (isWithInternetCondition) builder.AddCondition(new SystemCondition(SystemConditionType.InternetAvailable));
            builder.Register();
        }
    }
}
