﻿using System;
using Windows.Storage;

namespace Calendar.BackgroundAgent
{
    public static class AppStorageSettings
    {
        public static int StorageDay {
            get { return GetSettingByKey("StorageDayKey", 0); }
            set { SetSettingByKey("StorageDayKey", value); }
        }

        internal static int StorageDayOfNotification {
            get { return GetSettingByKey("StorageDayOfNotification", 0); }
            set { SetSettingByKey("StorageDayOfNotification", value); }
        }

        public static TimeSpan StorageNotificationTime {
            get { return GetSettingByKey("StorageNotificationTime", new TimeSpan(12, 0, 0)); }
            set { SetSettingByKey("StorageNotificationTime", value); }
        }

        public static bool IsPushNotificationEnbaled {
            get { return GetSettingByKey("IsPushNotificationEnbaled", true); }
            set { SetSettingByKey("IsPushNotificationEnbaled", value); }
        }

        private static T GetSettingByKey<T>(string key, T defaultValue)
        {
            var values = ApplicationData.Current.LocalSettings.Values;
            return values.ContainsKey(key) && values[key] is T ? (T)values[key] : defaultValue;
        }

        private static void SetSettingByKey(string key, object value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value;
        }
    }
}
