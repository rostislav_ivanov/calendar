﻿using System.Globalization;
using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.TileContent;
using NotificationsExtensions.ToastContent;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.System;
using Windows.UI.Notifications;

namespace Calendar.BackgroundAgent
{
    public sealed class UpdateApplicationTileTask : IBackgroundTask
    {
        async void IBackgroundTask.Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();
            try { await ExecuteInner(true); }
            finally { deferral.Complete(); }
        }

        internal static async void ExecuteBackgroundTask()
        {
            try { await ExecuteInner(false); }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        public static void SetUpPushNotification(bool isBackgroundAgentCall)
        {
            try
            {
                var now = DateTime.Now;
                var day = now.DayOfYear;
                var todayToday = Localization.LocalizedResources.GetDayDescripyionString("Today");
                var todayTitle = DateTime.Now.ToString("d MMMM ", new CultureInfo("ru-Ru"));
                var todayDescription = Localization.LocalizedResources.GetDayDescripyionString(todayTitle);

                if (AppStorageSettings.IsPushNotificationEnbaled && day != AppStorageSettings.StorageDayOfNotification)
                {
                    var notificatioDeliveryTime = now.Date.Add(AppStorageSettings.StorageNotificationTime);
                    if (notificatioDeliveryTime >= now)
                    {
                        var toastContent = ToastContentFactory.CreateToastText03();
                        toastContent.TextHeadingWrap.Text = todayToday + todayDescription;                   
                        toastContent.Duration = ToastDuration.Long;
                        var tostNotification = toastContent.CreateNotification();
                        if (isBackgroundAgentCall)
                            tostNotification.Activated += (s, e) => Launcher.LaunchUriAsync(new Uri("calendar://runApp"));

                        var recurringToast = new ScheduledToastNotification(tostNotification.Content,
                            notificatioDeliveryTime) { Id = "Recurring_Toast" };
                        ToastNotificationManager.CreateToastNotifier().AddToSchedule(recurringToast);
                        AppStorageSettings.StorageDayOfNotification = day;
                    }
                }
            }
            catch (Exception ex) { if (Debugger.IsAttached) Debugger.Break(); }
        }

        private static async Task ExecuteInner(bool isBackgroundAgentCall)
        {
            var now = DateTime.Now;
            var day = now.DayOfYear;
            var storageDay = AppStorageSettings.StorageDay;

            var todayTitle = DateTime.Now.ToString("d MMMM ", new CultureInfo("ru-Ru"));
            var todayDescription = Localization.LocalizedResources.GetDayDescripyionString(todayTitle);
            
            var date = new DateTime(DateTime.Now.Year, 1, 1).AddDays(day);
            var largeIconUrl = string.Format("ms-appx:///Assets/Images/{0}_{1}.jpg", 
                date.Month, date.Day.ToString().PadLeft(2, '0'));

            SetUpPushNotification(isBackgroundAgentCall);

            if (day == storageDay) return;

            var tileManager = TileUpdateManager.CreateTileUpdaterForApplication();
            tileManager.Clear();

            var notifications = tileManager.GetScheduledTileNotifications();
            foreach (var notification in notifications)
                tileManager.RemoveFromSchedule(notification);
            
          
#if WINDOWS_PHONE_APP          
            var tile150X150 = TileContentFactory.CreateTileSquare150x150PeekImageAndText01();        
            tile150X150.Image.Src = "ms-appx:///Assets/logo.png";
            tile150X150.Branding = TileBranding.None;  
          
            var tile150X150Back = TileContentFactory.CreateTileSquare150x150Text04();
            tile150X150Back.TextBodyWrap.Text = todayTitle + "\n" + todayDescription;
            tile150X150Back.Branding = TileBranding.None; 
            
            var tile71X71 = TileContentFactory.CreateTileSquare71x71Image();
            tile71X71.Image.Src = "ms-appx:///Assets/squareLogo.png";  
            tile71X71.Branding = TileBranding.None;
            tile150X150.Square71x71Content = tile71X71;

            var tile310X150Front = TileContentFactory.CreateTileWide310x150Image();
            tile310X150Front.Image.Src = "ms-appx:///Assets/widelogo.png";
            tile310X150Front.Square150x150Content = tile150X150;
            tile310X150Front.Branding = TileBranding.None;  

            var tile310X150Back = TileContentFactory.CreateTileWide310x150Text03();
            
            tile310X150Back.TextHeadingWrap.Text = todayTitle + "\n" + todayDescription;
            tile310X150Back.Square150x150Content = tile150X150Back;
            tile310X150Back.Branding = TileBranding.None;           
            tileManager.Update(tile310X150Front.CreateNotification());
            tileManager.Update(tile310X150Back.CreateNotification());
            tileManager.EnableNotificationQueueForWide310x150(true);
            tileManager.EnableNotificationQueueForSquare150x150(true);
#else
            var tileContent = TileContentFactory.CreateTileWide310x150Text03();
            tileContent.TextHeadingWrap.Text = todayTitle + "\n" + todayDescription;

            var dayBeforeTitle = DateTime.Now.AddDays(-1).ToString("d MMMM ", new CultureInfo("ru-Ru"));
            var dayBeforeDescription = Localization.LocalizedResources.GetDayDescripyionString(dayBeforeTitle);

            var twoDaysBeforeTitle = DateTime.Now.AddDays(-2).ToString("d MMMM ", new CultureInfo("ru-Ru"));
            var twoDaysBeforeDescription = Localization.LocalizedResources.GetDayDescripyionString(twoDaysBeforeTitle);          


            var squareContent = TileContentFactory.CreateTileSquare150x150Text04();
            squareContent.TextBodyWrap.Text = todayTitle + "\n" + todayDescription;

            tileContent.Square150x150Content = squareContent;

            var tile310X310 = TileContentFactory.CreateTileSquare310x310TextList02();
            tile310X310.TextWrap1.Text = String.Format(todayTitle + "\n" + todayDescription);
            tile310X310.TextWrap2.Text = String.Format(dayBeforeTitle + "\n" + dayBeforeDescription);
            tile310X310.TextWrap3.Text = String.Format(twoDaysBeforeTitle + "\n" + twoDaysBeforeDescription);
            tile310X310.Wide310x150Content = tileContent;
            var futureTile = new ScheduledTileNotification(tile310X310.GetXml(), DateTime.Now.AddSeconds(2)) { Id = "Id" };      
            TileUpdateManager.CreateTileUpdaterForApplication().AddToSchedule(futureTile);
#endif
            AppStorageSettings.StorageDay = day;
        }
    }
}
